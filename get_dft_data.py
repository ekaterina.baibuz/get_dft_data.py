import sys
sys.path.append("/wrk/baibuz/Tungsten/dft")
import vaspout_analysis
from vaspout_analysis import *
e0 = 55.3*1e-4 # eV/A  #8.854187817620e-12 * 1e10 # F/A

system_name = "3nn"
print "NAME OF THE SYSTEM:%s"%system_name, " change if wrong"
systems = ["lat","ref","saddle"]

folder_names= [ name for name in os.listdir(".") if (os.path.isdir(os.path.join(".", name)) and "field" in name)]
file_to_write_lat = open("dft_%s_lat.dat"%system_name,"w")
file_to_write_saddle = open("dft_%s_saddle.dat"%system_name,"w")
file_to_write_ref = open("dft_%s_ref.dat"%system_name,"w")
header = "#F [V/A] mu of the supercell [eA] corrections dft [eV] corrections calculated [eV] cell Volume [A^3] Energy [eV] z_{adatom} - z_{surface layer} [A] N kp [] System\n"
file_to_write_lat.write(header)
file_to_write_saddle.write(header)
file_to_write_ref.write(header)
fields_lat = []
fields_saddle = []
fields_ref = []
print_stuff = False
for field_folder in folder_names:
    subfolder_names= [ os.path.join(field_folder,name) for name in os.listdir(field_folder) if (os.path.isdir(os.path.join(field_folder, name)) and name in systems)]
    cases = subfolder_names    
    for system in subfolder_names:
        #print "system",system, subfolder_names
        subsubfolders = []
        dirnames = os.listdir(system)
        #print "dirnames", dirnames
        for name in dirnames:
            #print "name", name
            if os.path.isdir(os.path.join(system,name)):
		subsubfolders.append(os.path.join(system,name))
                    #print "subsubfolders",subsubfolders
        cases +=subsubfolders
    if print_stuff:
	    print "\n +++++++++++++++++ FIELD %s  +++++++++++++++++ \n"%field_folder 
    for system in cases:
	
	if if_converged(system,errms = False):
		if print_stuff:
			print "\n"
		indx =  [i for i,val in enumerate(system) if val=="/"]
		if len(indx)>1:
			f = system[indx[-2]:indx[-1]]
		else:
			f = system[indx[0]:]
		if print_stuff:
			if "lat" in f:
				print "LATTICE", f 
			if "saddle" in f:
				print "SADDLE", f
			if "ref":
				print "REFERENCE SYSTEM", f
			print "\n"
		mu = get_mu(system,errms  = False)
		dc_corr = get_corr(system,errms = False)
		e = get_E(system,errms = False)
		z = get_z_adatom(system,errms = False)
		V = get_V(system,errms = False)
		dc = mu*mu/(2*e0*V)
		charge_folder = os.path.join(system,"dipole")
		if os.path.exists(charge_folder):
			q = get_q(charge_folder,"281",errms = False)
		else:
			q = 0.0
		field = (get_flag_outcar(system,"EFIELD"))
		print "field from file", field
		if field==False:
			field = 0.0
		field = float(field)
		if field!=0.0:
			field = -field
		else:
			field = 0.0
		kp = get_flag_outcar(system,"NKPTS")
		ng = [get_flag_outcar(system,"NGX"),get_flag_outcar(system,"NGY"),get_flag_outcar(system,"NGZ")]
		LDipol = get_flag_outcar(system,"LDIPOL")
		IDIPOL = get_flag_outcar(system,"IDIPOL")
		ENCUT = get_flag_outcar(system,"ENCUT")
		EDIFF = get_flag_outcar(system,"EDIFF")
		
		if print_stuff:
			print "[E field] LDipol IDIPOL ENCUT EDIFFG K-points E[ev] mu[eA]"
			print field, LDipol, IDIPOL, ENCUT, EDIFF, kp, e,mu
		system_name = os.path.split(system)
		line_to_write = "%f %f %f %f %f %f %f %f %s %s %s %s %s %s\n"%(field,mu,dc_corr,dc,V, e,z,q,LDipol, IDIPOL, ENCUT, EDIFF, kp,system_name[-1])		
		print line_to_write

		if "lat" in system:
			file_to_write_lat.write(line_to_write)
			fields_lat.append(float(field))
		elif "saddle" in system:
			file_to_write_saddle.write(line_to_write)
			fields_saddle.append(float(field))
		elif "ref" in system:
			file_to_write_ref.write(line_to_write)
			fields_ref.append(float(field))
	else:
		print system, "not converged"
fields_lat.sort()
print "field [eV/A] lattice reference saddle "
for field in fields_lat:
	string = str(field) + " T "
	if field in fields_ref:
		string += " T "
	else:
		string += " F "
	if field in fields_saddle:
		string += " T "
	else:
		string += " F "
	string += "\n"
	print string

