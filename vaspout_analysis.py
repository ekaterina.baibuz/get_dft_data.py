import numpy as np
#import matplotlib
from scipy.integrate import simps
#from matplotlib import pyplot as plt
import sys
import re
import os
import math
from math import *
#import column_width
#setting parameters **************
font=30
lines=['b-','r-','g-','m-','k-','c-', 'y-', 'b--']


def check_incar(incar_file,tag):
    #reads in INCAR file and searches for the value of the tag       
    with open(incar_file) as f:
        INCAR_lines = f.readlines()
    for line in INCAR_lines:
        words = []
        words = line.split()
        if len(words)>0 and words[0].startswith(tag):
            # determing the value assigned to a tag
            if len(words)>=2:

                return words[2]
            else:

                match = re.match(r"([a-zA-Z]+)=([ a-zA-Z0-9_,.]+)", words[0], re.I)
                if match:
                    items = match.groups()
                    return items[1]
    return 0
class field_analyser:
    def __init__(self,field = 0.0,e_lattice = 0.0,mu_lattice = 0.0,e_saddle = 0.0,mu_saddle = 0.0):
        self.field = field
        self.mu_saddle = mu_saddle
        self.mu_lattice = mu_lattice
        self.e_saddle = e_saddle
        self.e_lattice = e_lattice
    def get_barrier(self):
        if self.e_saddle == 0 or self.e_lattice == 0:
            return 0.0
        else:
            return self.e_saddle - self.e_lattice
class readable:
    
    def __init__(self,filename):
        
        if ( "LOCPOT" in filename):
            self.mode='potential'
        elif ('CHGCAR' in filename or "BvAt" in filename):
            self.mode='charge'
        self.filename = filename
        self.fid=open(filename)
        self.readfile()
        self.get_elecperatom()
        self.fid.close()
        
    def readfile(self):
        f=self.fid
        f.readline()
        scale= float(f.readline().strip())
        self.cell=np.zeros(3)
        for i in range(3):
            line=f.readline()
            self.cell[i]=float(line.strip().split()[i])
        self.cell=self.cell*scale 
        f.readline()
        line=f.readline()
        self.Natoms=int(line.strip())
        line=f.readline()
        
        self.positions=np.zeros([self.Natoms,3]) # rescale to cartesian coordinates
        for i in range(self.Natoms):
            line=f.readline()
            self.positions[i,:]=self.cell*map(float,(line.strip().split()))

        line=f.readline()
        line=f.readline()
        self.Ngrid=map(int,(line.strip().split()))
    
        self.charge=np.zeros(np.prod(self.Ngrid))
    
        i=0
        #print self.Ngrid," ",np.prod(self.Ngrid)
        while i < np.prod(self.Ngrid):

            values = [float(val) for val in f.readline().split()]
            for val in values:
                self.charge[i]=val
                i += 1
        self.charge=self.charge.reshape(self.Ngrid,order='F')
        self.grid_spacing=self.cell/self.Ngrid
        if self.mode=='charge':
            self.charge=self.charge/np.prod(self.cell) # np.prod(self.cell) is a colume
            
        self.x=self.grid_spacing[0]*np.arange(self.Ngrid[0]) # x cartesian coordinates of grid points 
        self.y=self.grid_spacing[1]*np.arange(self.Ngrid[1]) # y cartesian coordinates of grid points
        self.z=self.grid_spacing[2]*np.arange(self.Ngrid[2]) # z cartesian coordinates of grid points

    def axiset(self,ax):
        ax.set_xlabel(r'$z[\AA]$', fontsize=18)
        if self.mode=='charge':
            ax.set_ylabel(r'$\rho[e/\AA^3]$', fontsize=18)
            ax.set_title(r'xy-averaged charge density $\rho$', fontsize=20)
        else:
            ax.set_ylabel(r'$V [eV]$', fontsize=18)
            ax.set_title(r'xy-averaged potential', fontsize=20)
    def get_zplot(self):
        #returns z
        #zinterface = max(self.positions[:,2])
        #zplot=self.grid_spacing[2]*np.arange(self.Ngrid[2])-zinterface
        #zplot = self.grid_spacing[2]*np.arange(self.Ngrid[2])    
        return self.z

    def get_cplot_average(self):
        #returns charge or potential
        cplot = np.average(self.charge,axis=(0,1))
        return cplot

    def get_cplot_xyfixed(self,index,threshold = None):
        #returns charge or potential averaged by xy coordinates within the fixed interval of x and y of an atom 
        charge_mask = np.zeros(self.Ngrid)

        x_atom = self.positions[index,0]
        y_atom = self.positions[index,1]
        z_atom = self.positions[index,2]
        cplot = []
        if threshold is None:
            threshold_x = 0
            threshold_y = 0
        else:
            threshold_x = self.cell[0]/(self.Natoms)
            threshold_y = self.cell[1]/(self.Natoms)
        print "threshold ", threshold_x, " ",threshold_y
        print "adatom coordinates: ", x_atom, " ",y_atom, " ",z_atom

        for i in range(0,self.Ngrid[0]):
            for j in range(0,self.Ngrid[1]):
                if fabs(self.x[i]-x_atom)<=threshold_x and fabs(self.y[j]-y_atom)<=threshold_y:
                    for k in range(0,self.Ngrid[2]):
                        charge_mask[i,j,k] = 1
                         #cplot.append(charge[i,j,k])
        cplot =  np.average(self.charge,axis=(0,1),weights = charge_mask)
        return cplot

    def plotxyaverage(self,axis,lbl=None,line='b-'):
        zinterface = max(self.positions[:,2])
        zplot=self.grid_spacing[2]*np.arange(self.Ngrid[2])-zinterface
        cplot=np.average(self.charge,axis=(0,1))
        axis.plot(zplot,cplot,line,linewidth=2,markersize=3,label=lbl)

   

    def contour_zaverage(self,axis,lbl=None):
        x=self.grid_spacing[0]*np.arange(self.Ngrid[0])
        y=self.grid_spacing[1]*np.arange(self.Ngrid[1])
        xplot,yplot = np.meshgrid(x,y)
        zplot = np.average(self.charge,axis=2)
        axis.contourf(xplot,yplot,zplot)

    def plotatomic(self,atoms_per_layer,axis,lbl=None,line=None):
        if line is None:
            line='b-'
        layers=self.Natoms/atoms_per_layer
        self.zlayers=np.zeros(layers)
        for i in range(layers):
            self.zlayers[i]=np.average(self.positions[i*atoms_per_layer:(i+1)*atoms_per_layer-1,2])
        for z in self.zlayers:
            axis.plot([z, z],[axis.get_ylim()[0], axis.get_ylim()[1]],line,label=lbl,linewidth=3)
    
    def get_elecperatom(self):
        #find electrons per atom from outcar
        for i in range(len(self.filename)):
            if self.filename[-i-1] == '/':
                break
        if i==-1+len(self.filename): i=0
        outcar = open(self.filename[0:-i] + 'OUTCAR', 'r')
        for line in outcar.xreadlines():
            if (line.find('NELECT')>-1):
                self.e_per_atom = float(line.strip().split()[2])/self.Natoms
                return self.e_per_atom
        
    def net_charge(self):
        negcharge=simps(simps(simps(self.charge,self.z),self.y),self.x)
        poscharge=self.Natoms* self.e_per_atom
        netcharge=poscharge-negcharge
        return (netcharge,negcharge)
    
    def dipole_moment(self, R = np.array([0,0,0]), atomNo = 0):
        #calculates the dipole moment of the charge of a certain atom
        #the read file should be after bader analysis
        if (atomNo > 0):
            r0 = self.positions[atomNo - 1]#position of the interested atom
        else:
            r0 = R

        xvector = np.copy(self.x)
        if r0[0]<self.cell[0]/3:
            xvector[self.Ngrid[0]/2:] -=self.cell[0]
            #xvector = np.roll(xvector,self.Ngrid[0]/2)
        
        yvector = np.copy(self.y)
        if r0[1]<self.cell[1]/3:
            yvector[self.Ngrid[1]/2:] -=self.cell[1]
            #yvector = np.roll(yvector,self.Ngrid[1]/2)
        zvector = np.copy(self.z)
        if r0[2]<self.cell[2]/4:
            zvector[self.Ngrid[2]/2:] -=self.cell[2]
            #zvector = np.roll(zvector,self.Ngrid[2]/2)
        
        self.xgrid, self.ygrid, self.zgrid = np.meshgrid(xvector, yvector, zvector, indexing = 'ij')
        
        px =  sum(sum(sum((self.xgrid - r0[0]) * self.charge))) * np.prod(self.grid_spacing)
        py = sum(sum(sum((self.ygrid - r0[1]) * self.charge))) * np.prod(self.grid_spacing)
        pz = sum(sum(sum((self.zgrid - r0[2]) * self.charge))) * np.prod(self.grid_spacing)
        
        if (atomNo <= 0):
            px -= sum(self.positions[0] - r0[0]) * self.e_per_atom
            py -= sum(self.positions[1] - r0[1]) * self.e_per_atom
            pz -= sum(self.positions[2] - r0[2]) * self.e_per_atom
        
        return np.array([-px,-py,-pz])

    def dipole_moment_whole(self, R):
        #calculates the dipole moment of the whole system 
        r0 = R #reference point; position of the center of the mass

        xvector = np.copy(self.x)
        if r0[0]<self.cell[0]/3:
            xvector[self.Ngrid[0]/2:] -=self.cell[0]
            #xvector = np.roll(xvector,self.Ngrid[0]/2)
        
        yvector = np.copy(self.y)
        if r0[1]<self.cell[1]/3:
            yvector[self.Ngrid[1]/2:] -=self.cell[1]
            #yvector = np.roll(yvector,self.Ngrid[1]/2)
        zvector = np.copy(self.z)
        if r0[2]<self.cell[2]/4:
            zvector[self.Ngrid[2]/2:] -=self.cell[2]
            #zvector = np.roll(zvector,self.Ngrid[2]/2)
        self.xgrid, self.ygrid, self.zgrid = np.meshgrid(xvector, yvector, zvector,indexing = 'ij')
        # total charge of slab
        
        px=sum(sum(sum((self.xgrid - r0[0]) * self.charge))) * np.prod(self.grid_spacing) - sum(self.positions[0] - r0[0])*charge_per_atom
        py=sum(sum(sum((self.ygrid - r0[0]) * self.charge))) * np.prod(self.grid_spacing) - sum(self.positions[1] - r0[1])*charge_per_atom
        pz=sum(sum(sum((self.zgrid - r0[0]) * self.charge))) * np.prod(self.grid_spacing) - sum(self.positions[2] - r0[2])*charge_per_atom
        return np.array([px,py,pz])
       
    def surface_atoms_charge(self,Efield):
        sigma=5.527e-3*Efield
        net_surface_charge=sigma*self.cell[0]*self.cell[1]
        return net_surface_charge
    
    
def fit_linear(fields,dipoles,plot = False):
    x = []
    y = []
    for i in range(len(dipoles)):
        if dipoles[i] !=0.0:
            x.append(fields[i])
            y.append(dipoles[i])
    fit = np.polyfit(x,y,1)
    polyfitvals = np.polyval(fit,x)
    mu = fit[1]
    alpha = fit[0]
    if plot == True:
        plt.plot(x,polyfitvals,"--")
    return mu,alpha
def fit_line(delta_fields,barriers,field,plot = True, color = "k"):
    fit = np.polyfit(delta_fields,barriers,1)
    polyfitvals = np.polyval(fit,delta_fields)
    E0 = fit[1]
    c = fit[0]
    if plot == True:
        plt.plot(delta_fields,polyfitvals,"--",label=r" %6.1fGV/m fit $[E_{b}=%6.3f+%6.3f\Delta F]$"%(field,E0,c),color = color)
    else:
        plt.plot(delta_fields,polyfitvals,"--",color = color)
    print r" %6.1fGV/m fit $[E=%6.3f+%6.3f\Delta F]$"%(field,E0,c)

def fit_parabola(fields,dipoles,plot = False, color = "k"):
    x = []
    y = []
    for i in range(len(dipoles)):
        if dipoles[i] !=0.0:
            x.append(fields[i])
            y.append(dipoles[i])
    fit = np.polyfit(x,y,2)
    print fit
    polyfitvals = np.poly1d(fit)
    print polyfitvals
    mu = fit[2]
    alpha = fit[1]
    gamma = fit[0]
    polyfit_vals = polyfitvals(np.linspace(min(fields)-0.5,max(fields)+0.5,100))

    if plot == True:
        plt.plot(np.linspace(min(fields)-0.5,max(fields)+0.5,100),polyfitvals(np.linspace(min(fields)-0.5,max(fields)+0.5,100)),"--",color = color)

    return mu,alpha,gamma

def u_p_f(field,mu,alpha):
    # function calculates change in energy due to dipole moment
    return (mu + field*alpha/2)*field
def elecpot_vs_effpot(elecpot_folder,effpot_folder):
    #!# function plots xy-averaged electrostatic potential and effective potential 
    #!# written in LOCPOT files in provided folders
    fig = plt.figure()
    ax = fig.add_subplot(111)
    tags = ["LVTOT","LVHAR"]
    folders = [elecpot_folder,effpot_folder]
    for folder in folders:
        print folder, " :"
        for tag in tags:
            tag_value = check_incar(os.path.join(folder,"INCAR"),tag)
            if tag_value==0:
                print "INCAR doesn't specify %s option"%(tag)
            #else:
               # print "%s = %s"%(tag,tag_value)
        filename = os.path.join(folder,"LOCPOT")
        if not os.path.exists(filename):
            print "LOCPOT DOES NOT EXIST CHECK: ", filename
            return 0
        field = 10*float(check_incar(os.path.join(folder,"INCAR"),"EFIELD"))
        if folder == elecpot_folder:
            label = "electrostatic potential, %s GV/m"%str(field)
            line_type = "--"            
        elif folder == effpot_folder:
            label = "effective potential, %s GV/m"%str(field)
            line_type = "-"
        chpot=readable(filename)
        chpot.axiset(ax)
        chpot.plotxyaverage(ax,lbl=label,line=line_type)  
        chpot.plotatomic(2,ax,line='r--')
        plt.legend(loc='lower right',prop={'size':8})

def change_in_locpot_with_field(file_without_field,file_with_field,lbl=None):
    #!# function plots change in potential introduced by field;
    #!# takes folders of with field simulations and without field simulations as input
    fig = plt.figure()
    ax = fig.gca()
    if not os.path.exists(file_without_field):
        print "LOCPOT DOES NOT EXIST CHECK: ", file_without_field
        return 0

    locpot_withoutfield=readable(file_without_field)
    locpot_withoutfield.axiset(ax)
    #zinterface = max(self.positions[:,2])
    #zplot=self.grid_spacing[2]*np.arange(self.Ngrid[2])-zinterface
    #cplot=np.average(self.charge,axis=(0,1))
    zplot_without = locpot_withoutfield.get_zplot()
    cplot_without = locpot_withoutfield.get_cplot(ax)

    if not os.path.exists(file_with_field):
        print "LOCPOT DOES NOT EXIST CHECK: ", file_with_field
        return 0
    locpot_field=readable(file_with_field)
    locpot_field.axiset(ax)
    zplot_field = locpot_field.get_zplot()
    cplot_field = locpot_field.get_cplot(ax)
 
    zplot_diff = zplot_field-zplot_without
    cplot_diff = cplot_field-cplot_without
    ax.plot(zplot_diff,cplot_diff,linewidth=2,markersize=3,label=lbl)
    #ax.plot(zplot_field,cplot_field)
    #ax.plot(zplot_without,cplot_without)
    locpot_field.plotatomic(32,ax,line='k--')
    locpot_withoutfield.plotatomic(32,ax,line='r--')
    plt.legend(loc='lower right',prop={'size':8})

def change_in_charge_with_field(folder_without_filed,folder_with_filed,lbl=None):
    #!# function plots change in charge introduced by field;
    #!# takes folders of with field simulations and without field simulations as input
    ### CAN BE COMBINED WITH change_in_locpot_with_field
    fig = plt.figure()
    ax = fig.gca()
    if not os.path.exists(os.path.join(folder_without_filed,"CHGCAR")):
        print "CHGCAR DOES NOT EXIST CHECK: ", os.path.join(folder_without_filed,"CHGCAR")
        return 0
    CHGCAR_withoutfield=readable(os.path.join(folder_without_filed,"CHGCAR"))
    CHGCAR_withoutfield.axiset(ax)
    zplot_without = CHGCAR_withoutfield.get_zplot()
    cplot_without = CHGCAR_withoutfield.get_cplot(ax)

    if not os.path.exists(os.path.join(folder_with_filed,"CHGCAR")):
        print "CHGCAR DOES NOT EXIST CHECK: ", os.path.join(folder_with_filed,"CHGCAR")
        return 0
    CHGCAR_field=readable(os.path.join(folder_with_filed,"CHGCAR"))
    CHGCAR_field.axiset(ax)
    zplot_field = CHGCAR_field.get_zplot()
    cplot_field = CHGCAR_field.get_cplot(ax)
    field1 = check_incar(os.path.join(folder_without_filed,"INCAR"),"EFIELD")
    if field1 == 0:
        lbl1 = "0.0"
    else:
        lbl1 = str(field1)
    field2 = check_incar(os.path.join(folder_with_filed,"INCAR"),"EFIELD")   
    if field2 == 0:
        lbl2 = "0.0"
    else:
        lbl2 = str(field2)
    if lbl is not None:
        lbl = "%s\Charge difference between %s and %s fields"%(lbl,lbl1,lbl2)
    else:
        lbl = "Charge difference between %s and %s fields"%(lbl1,lbl2)
    zplot_diff = zplot_without-zplot_field
    cplot_diff = cplot_without-cplot_field
    ax.plot(zplot_without,cplot_diff,linewidth=2,markersize=3,label=lbl)
    #ax.plot(zplot_field,cplot_field)
    #ax.plot(zplot_without,cplot_without)
    CHGCAR_field.plotatomic(2,ax,line='k--')
    CHGCAR_withoutfield.plotatomic(2,ax,line='r--')
    plt.legend(loc='lower right',prop={'size':8})
def dy_dx(xplot,yplot):
    return np.gradient(yplot)/np.gradient(xplot)


def electric_field(file):
    fig = plt.figure()
    ax = fig.gca()
    if not os.path.exists(file):
        print "FILE DOES NOT EXIST CHECK: ", file
        return 0
    output=readable(file)
    output.axiset(ax)
    xplot = output.get_zplot()
    yplot = output.get_cplot(ax)
    fieldplot = dy_dx(xplot,yplot)
    ax.plot(xplot,yplot,label="potential")
    ax.plot(xplot,fieldplot,label="field")
    output.plotatomic(2,ax,line='k--')
    plt.legend(loc="best")

def change_in_internalfield_with_field(folder_without_filed,folder_with_filed,lbl=None):
    #!# function plots change in potential introduced by field;
    #!# takes folders of with field simulations and without field simulations as input
    fig = plt.figure()
    ax = fig.gca()
    if not os.path.exists(os.path.join(folder_without_filed,"LOCPOT")):
        print "LOCPOT DOES NOT EXIST CHECK: ", os.path.join(folder_without_filed,"LOCPOT")
        return 0
    locpot_withoutfield=readable(os.path.join(folder_without_filed,"LOCPOT"))
    locpot_withoutfield.axiset(ax)
    zplot_without = locpot_withoutfield.get_zplot()
    cplot_without = locpot_withoutfield.get_cplot(ax)

    if not os.path.exists(os.path.join(folder_with_filed,"LOCPOT")):
        print "LOCPOT DOES NOT EXIST CHECK: ", os.path.join(folder_with_filed,"LOCPOT")
        return 0
    locpot_field=readable(os.path.join(folder_with_filed,"LOCPOT"))
    locpot_field.axiset(ax)
    zplot_field = locpot_field.get_zplot()
    cplot_field = locpot_field.get_cplot(ax)
    field1 = check_incar(os.path.join(folder_without_filed,"INCAR"),"EFIELD")
    if field1 == 0:
        lbl1 = "0.0"
    else:
        lbl1 = str(field1)
    field2 = check_incar(os.path.join(folder_with_filed,"INCAR"),"EFIELD")   
    if field2 == 0:
        lbl2 = "0.0"
    else:
        lbl2 = str(field2)
    if lbl is not None:
        lbl = "%s\nPotential difference between %s and %s fields"%(lbl,lbl1,lbl2)
    else:
        lbl = "Potential difference between %s and %s fields"%(lbl1,lbl2)
    cplot_diff = cplot_without-cplot_field
    field_diff = dy_dx(zplot_without,cplot_diff)
    ax.plot(zplot_without,cplot_diff,linewidth=2,markersize=3,label="potential")
    ax.plot(zplot_without,field_diff,linewidth=2,markersize=3,label="field")
    locpot_field.plotatomic(2,ax,line='k--')
    locpot_withoutfield.plotatomic(2,ax,line='r--')
    plt.legend(loc='best')

def get_energy(folder,peratom = False):
    ## function reads OSZICAR in provided folder and finds E0;
    ## If peratom is True, number of atoms is read from INCAR in provided folder.
    ## Returns energy or energy per atom correspondingly.
    E0 = 0.0
    if peratom:
        with open(os.path.join(folder,"POSCAR")) as f:
            poscar_lines = f.readlines()
        divided_line = poscar_lines[6].split()
        number_atoms = float(divided_line[0])

    with open(os.path.join(folder,"OSZICAR")) as f:
        stdout_lines = f.readlines()
    for line in stdout_lines:
        words = []
        words = line.split()
        i = 0
        while i < len(words):
            if words[i]=="E0=":
                E0 = float(words[i+1])
                i +=1
            i +=1
    if peratom:
        return E0/number_atoms
    else:
		 return E0


def absorption_energy(reference_folder,folder_with_adatom):
    # function returns absorption energy, 
    #calculated as a difference between energy per atom of slab system and system with adatom
    E0_reference = get_energy(reference_folder)
    E0_adatom = get_energy(folder_with_adatom)
    return E0_reference-E0_adatom

def quickplot(files):
   #quickly plots listed xy averaged locpot or chgcar
    fig = plt.figure()
    ax = fig.gca()

    i=0
    for filename in files:
        chpot=readable(filename)
        chpot.axiset(ax)
        chpot.plotxyaverage(ax,line=lines[i])
        i+=1

    #plt.legend(loc='best')
    #plt.show()
def get_file_list(d,filename):
    #Makes a list of all folders in directory pwd and adds /filename in the end
    dirs=filter(lambda x: os.path.isdir(os.path.join(d, x)), os.listdir(d))
    for i in range(len(dirs)):
        dirs[i] = dirs[i] + '/' + filename
    dirs.sort()
    return dirs

def get_charge_average(file):

    if not os.path.exists(file):
        print "FILE DOES NOT EXIST CHECK: ", file
        return 0
    output=readable(file)
    xplot = output.get_zplot()
    yplot = output.get_cplot_average()
    return yplot,xplot

def get_charge_fixed(file,index,threshold = None):

    if not os.path.exists(file):
        print "FILE DOES NOT EXIST CHECK: ", file
        return 0
    output=readable(file)
    xplot = output.get_zplot()
    if threshold is None:
        yplot = output.get_cplot_xyfixed(index-1)
    else:
        yplot = output.get_cplot_xyfixed(index-1,threshold)
    return yplot,xplot

   
def get_z(file,errms = True):        
    if not os.path.exists(file):
	if errms:
	        print "FILE DOES NOT EXIST CHECK: ", file
        return 0
    output=readable(file)
    xplot = output.get_zplot()
    return xplot

def average(list):
        av = 0.0
        for el in list:
                av +=el
def get_contcar(results_folder,errms = True):
        contcar = os.path.join(results_folder,"CONTCAR")
        if os.path.exists(contcar):
                with open(contcar, "r") as f:
                        contcar_lines = f.readlines()
                return contcar_lines
        else:
		if errms:
	                print contcar, " does not exist"
                return False

def get_box(results_folder):
        a = [0.0,0.0,0.0,0.0]
        contcar_lines = get_contcar(results_folder)
        if contcar_lines != False:
                a[0] = float(contcar_lines[1])
                a[1] = [float(i) for i in contcar_lines[2].split()]
                a[2] = [float(i) for i in contcar_lines[3].split()]
                a[3] = [float(i) for i in contcar_lines[4].split()]

                return a
        else:
                return False
def get_coordinates(results_folder):
        contcar_lines = get_contcar(results_folder)
        #print contcar_lines
        if contcar_lines != False:
                x = []
                y = []
                z = []
                a = get_box(results_folder)
                a0 = a[0]
                a3 = a[3]
                a1 = a[1]
                a2 = a[2]
                print a0
                print a1
                print a2
                print a3
                for line in contcar_lines:
                        words = line.split()
                        if len(words)==6:
                                x.append(float(words[0])*a0*a1[0])
                                y.append(float(words[1])*a0*a2[1])
                                z.append(float(words[2])*a0*a3[2])
                return x,y,z
        else:
                return False
def get_charges(results_folder,errms = True):
        charges = []
        acf = os.path.join(results_folder,"ACF.dat")
        if os.path.exists(acf):
                with open(acf, "r") as f:
                        acf_lines = f.readlines()
                header = acf_lines[0].split()
                for line in acf_lines:
                        items = line.split()
                        if len(items) == 7 and [s for s in items if s.isdigit()]:
                                charges.append(items[header.index("CHARGE")])
                return charges
        else:
		if errms:
	                print acf, " does not exist"
                return 0.0

def get_q(results_folder, atom_id,errms = True):
        q = 0
	acf = os.path.join(results_folder,"ACF.dat")
        if os.path.exists(acf):
                with open(acf,"r") as f:
                        acf_lines = f.readlines()
                for line in acf_lines:
                        if len(line)>=7:
                                words = line.split()
                                if str(atom_id) in words:
                                        q = float(words[4])
                                        
        else:
		if errms:
	             	print acf, "doesn't exist"
        return q

def get_E( results_folder,errms = True ):
        E0 = 0.0
        line_to_search = "energy without entropy"
        word_to_search = "energy(sigma->0)"
        outcar = os.path.join(results_folder,"OUTCAR")
        if os.path.exists(outcar):
                with open(outcar) as f:
                        lines = f.readlines()
                for line in lines:
                        if line_to_search in line:
                                items = line.split()
                                for i in range(0,len(items)):
                                        if word_to_search in items[i]:
                                                if items[i+1]== "=":
                                                        E0 = float(items[i+2])
                                                else:
                                                        string = items[i+1]

                                                        E0 = float(string[1:])
        else:
		if errms:
	                print os.path.join(results_folder,"OUTCAR"), "foesn't exists, returns 0"
                E0 = 0.0
        return E0

def get_corr(results_folder, errms = True):
        corr = 0.0
        line_to_search = "dipol+quadrupol energy correction"
        #line_to_search = "added-field ion interaction"
        word_to_search = "correction"
        #word_to_search = "interaction"
        outcar = os.path.join(results_folder,"OUTCAR")
        if os.path.exists(outcar):
                with open(outcar) as f:
                        lines = f.readlines()
                for line in lines:
                        if line_to_search in line:
                                items = line.split()
                                for i in range(0,len(items)):
                                        if word_to_search in items[i]:
                                                corr = float(items[i+1])
        else:
		if errms:
	                print os.path.join(results_folder,"OUTCAR"), "foesn't exists, returns 0"
                corr = 0.0
        return corr
def get_mu( results_folder,errms = True ):
#in the convention where positive dipole is upwards, so -
        E0 = 0.0
        line_to_search = "dipolmoment"
        word_to_search = "electrons"
        outcar = os.path.join(results_folder,"OUTCAR")
        if os.path.exists(outcar):
                with open(outcar) as f:
                        lines = f.readlines()
                for line in lines:
                        if line_to_search in line:
                                items = line.split()
                                for i in range(0,len(items)):
                                        if word_to_search in items[i]:
                                                    E0 = -float(items[i-1])

        else:
		if errms:
	                print os.path.join(results_folder,"OUTCAR"), "foesn't exists, returns 0"
                E0 = 0.0
        return E0
def get_V( results_folder,errms = True ):
        V = 0.0
        line_to_search = "volume of cell"
        word_to_search = ":"
        outcar = os.path.join(results_folder,"OUTCAR")
        if os.path.exists(outcar):
                with open(outcar) as f:
                        lines = f.readlines()
                for line in lines:
                        if line_to_search in line:
                                items = line.split()
                                for i in range(0,len(items)):
                                        if word_to_search in items[i]:
                                                    V = float(items[i+1])

        else:
		if errms:
	                print os.path.join(results_folder,"OUTCAR"), "foesn't exists, returns 0"
                V = 0.0
        return V
def get_layers(results_folder,errms = True):
        contcar_lines = get_contcar(results_folder,errms)
        if contcar_lines != False:
                x = []
                y = []
                z = []
                a = get_box(results_folder)
                a0 = a[0]
                a3 = a[3]
                for line in contcar_lines:
                        words = line.split()
                        if len(words)==6:
                                z.append(float(words[2])*a0*a3[2])
                mask = [True] * len(z)

                layers_z = []
                for i in range(len(z)):
                        if mask[i]==True:
                                layer = []
                                layer.append(z[i])
                                mask[i] = False
                                for j in range(i+1,len(z)):
                                        z2 = z[j]
                                        if fabs(z[i]-z2) <0.5:
                                                layer.append(z2)
                                                mask[j] = False
                                layers_z.append(sum(layer)/len(layer))
                layers_z = sorted(layers_z, key=float)
                return layers_z
        else:
                return False
def get_z_adatom(results_folder,errms = True):
        layers_z = get_layers(results_folder,errms)
	if layers_z != False:
	        return layers_z[-1]-layers_z[-2]
	return 0.0
def if_converged(results_folder,errms = True):
	outcar = os.path.join(results_folder,"OUTCAR")
	line_to_search = "reached required accuracy"
	converged = False
	if os.path.exists(outcar):
                with open(outcar) as f:
                        lines = f.readlines()
                for line in lines:
                        if line_to_search in line:
                                converged = True
				continue	
		
        else:
		if errms:
	             	print os.path.join(results_folder,"OUTCAR"), "foesn't exists, returns 0"
                converged = False
        return converged

def if_writing_wavecar(results_folder,errms = True):
        outcar = os.path.join(results_folder,"OUTCAR")
        line_to_search = "writing wavefunctions"
        converged = False
        if os.path.exists(outcar):
                with open(outcar) as f:
                        lines = f.readlines()
                for line in lines:
                        if line_to_search in line:
                                converged = True
                                continue     	

        else:
             	if errms:
                        print os.path.join(results_folder,"OUTCAR"), "foesn't exists, returns 0"
                converged = False
        return converged
	
def get_flag_outcar(results_folder,flag):
	flag_value = False
	line_to_search = flag
	outcar = os.path.join(results_folder,"OUTCAR")
	if os.path.exists(outcar):	
		with open(outcar) as f:
                        lines = f.readlines()
		for line in lines:
                        if line_to_search in line:
				items = line.split()
                                for i in range(1,len(items)):
					if items[i-1] == line_to_search:
						char = items[i] 
                     				if "=" in char:
							if len(char)>1:
								char = items[i]
								flag_value = char[1:]
							else:
								flag_value = items[i+1]
	return flag_value

